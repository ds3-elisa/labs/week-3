# Music API
### Uma API REST feita em Spring Boot

A Music API é uma interface de programação construída utilizando o framework Spring Boot e hospedada com o banco de dados PostgreSQL online através da ferramenta Neon Serverless Postgres.

As ferramentas utilizadas para o desenvolvimento da API foram:
- Spring Boot e o Spring Initializr
- IntelliJ Ultimate como IDE
- Postman
- Neon Serverless


## Requisitos funcionais
- **Inclusão de Músicas:** A API deve disponibilizar um endpoint para adicionar novas músicas à biblioteca. Os dados a serem incluídos devem incluir, no mínimo, título, artista, gênero e duração da música.
- **Exclusão de Músicas:** Deve ser possível excluir músicas da biblioteca através de um endpoint dedicado.
- **Atualização de Músicas:**  A API deve permitir a atualização dos detalhes de uma música existente. Os usuários devem poder modificar informações como artista, gênero e duração através de um endpoint específico.
- **Consulta de Músicas:** Deve ser possível consultar a biblioteca de músicas de várias maneiras, como pesquisas por título, artista, gênero, entre outros critérios. A API deve fornecer endpoints adequados para atender a essas consultas.

## Arquitetura
- **Camada de Controller:** Responsável por receber as requisições HTTP, validar os dados de entrada e encaminhar as chamadas para os serviços apropriados.
- **Camada de Service:** Contém a lógica de negócios da aplicação. Aqui, as operações de inclusão, exclusão, alteração e consulta de produtos devem ser implementadas.
- **Camada de Repository:** Responsável pela interação com o banco de dados ou o Map Java que armazena os dados do estoque. Todas as operações de acesso aos dados devem ser encapsuladas nesta camada.

## Ferrementas

- [Spring Boot](https://spring.io/projects/spring-boot) - Framework Java.
- [Spring Initializr](https://start.spring.io/) - Gerador de projetos Spring.
- [IntelliJ Ultimate](https://www.jetbrains.com/pt-br/idea/) - IDE Java.
- [Postman](https://www.postman.com/) - Aplicação para teste de APIs.
- [Neon Serverless](https://neon.tech/) - Versão em nuvem do PostgreSQL.