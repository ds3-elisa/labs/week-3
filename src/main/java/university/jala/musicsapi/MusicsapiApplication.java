package university.jala.musicsapi;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

@SpringBootApplication
public class MusicsapiApplication {

	public static void main(String[] args) {
		SpringApplication.run(MusicsapiApplication.class, args);
	}

}
