package university.jala.musicsapi.models;

import jakarta.persistence.*;
import jakarta.persistence.Id;
import org.springframework.hateoas.RepresentationModel;

import java.io.Serializable;
import java.util.UUID;

@Entity
@Table(name = "TB_MUSICS")
public class MusicModel extends RepresentationModel<MusicModel> implements Serializable {
    private static final long serialVersionUID = 1L;

    @Id
    @GeneratedValue(strategy= GenerationType.AUTO)
    private UUID idMusic;
    private String genre;
    private String title;
    private String artist;
    private String musicTime;

    public UUID getIdMusic() {
        return idMusic;
    }

    public void setIdMusic(UUID idMusic) {
        this.idMusic = idMusic;
    }

    public String getGenre() {
        return genre;
    }

    public void setGenre(String genre) {
        this.genre = genre;
    }

    public String getTitle() {
        return title;
    }

    public void setTitle(String title) {
        this.title = title;
    }

    public String getArtist() {
        return artist;
    }

    public void setArtist(String artist) {
        this.artist = artist;
    }

    public String getMusicTime() {
        return musicTime;
    }

    public void setMusicTime(String musicTime) {
        this.musicTime = musicTime;
    }
}
