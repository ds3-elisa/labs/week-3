package university.jala.musicsapi.controller;

import jakarta.validation.Valid;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;
import university.jala.musicsapi.dtos.MusicRecordDto;
import university.jala.musicsapi.models.MusicModel;
import university.jala.musicsapi.services.MusicService;

import java.util.List;
import java.util.UUID;

@RestController
public class MusicController {

    private final MusicService musicService;
    public MusicController(MusicService musicService) {
        this.musicService = musicService;
    }

    @PostMapping("/allmusics")
    public ResponseEntity<MusicModel> postMusic(@RequestBody @Valid MusicRecordDto musicRecordDto) {
        MusicModel musicModel = musicService.createMusic(musicRecordDto).getBody();
        return ResponseEntity.status(HttpStatus.CREATED).body(musicModel);
    }

    @GetMapping("/allmusics")
    public ResponseEntity<List<MusicModel>> getAllMusics() {
        List<MusicModel> musicList = musicService.searchAllMusics().getBody();
        return ResponseEntity.status(HttpStatus.OK).body(musicList);
    }

    @GetMapping("/allmusics/genres")
    public ResponseEntity<List<String>> getAllGenres() {
        List<String> genres = musicService.searchAllGenres();
        return ResponseEntity.status(HttpStatus.OK).body(genres);
    }

    @GetMapping("/allmusics/genres/{genre}")
    public ResponseEntity<List<MusicModel>> getMusicByGenre(@PathVariable("genre") String genre) {
        String lowercaseArtistName = genre.toLowerCase();
        List<MusicModel> musicList = musicService.searchMusicByGenre(genre);
        return ResponseEntity.status(HttpStatus.OK).body(musicList);
    }

    @GetMapping("/allmusics/artists/{artist}")
    public ResponseEntity<List<MusicModel>> getMusicByArtist(@PathVariable("artist") String artist) {
        String lowercaseArtistName = artist.toLowerCase();
        String formattedArtist = artist.toLowerCase().replace(" ", "-");
        List<MusicModel> musicList = musicService.searchMusicByArtist(artist);
        return ResponseEntity.status(HttpStatus.OK).body(musicList);
    }

    @GetMapping("/allmusics/sorted")
    public ResponseEntity<List<MusicModel>> getAllMusicsSortedByName() {
        List<MusicModel> musicList = musicService.getAllMusicsSortedByTitle();
        return ResponseEntity.status(HttpStatus.OK).body(musicList);
    }

    @GetMapping("/allmusics/{id}")
    public ResponseEntity<Object> getMusic(@PathVariable("id") UUID id) {
        return musicService.searchMusic(id);
    }

    @PutMapping("/allmusics/{id}")
    public ResponseEntity<Object> updateMusic(@PathVariable(value = "id") UUID id,
                                                @RequestBody @Valid MusicRecordDto musicRecordDto) {

        MusicModel updateMusic = musicService.updateMusic(id, musicRecordDto);
        if (updateMusic == null) {
            return ResponseEntity.status(HttpStatus.NOT_FOUND).body("Music Not Found");
        }
        return ResponseEntity.ok(updateMusic);
    }

    @DeleteMapping("/allmusics/{id}")
    public ResponseEntity<Object> deleteProduct(@PathVariable(value="id") UUID id) {
        boolean deleted = musicService.deletedMusic(id);
        if (!deleted) {
            return ResponseEntity.status(HttpStatus.NOT_FOUND).body("Music Not Found");
        }
        return ResponseEntity.status(HttpStatus.OK).body("Music Deleted Successfully");
    }
}
