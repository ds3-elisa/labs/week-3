package university.jala.musicsapi.dtos;

import jakarta.validation.constraints.NotBlank;
import jakarta.validation.constraints.NotNull;

import java.math.BigDecimal;

public record MusicRecordDto(@NotBlank String genre, @NotBlank String title, @NotBlank String artist, @NotBlank String musicTime) {
}