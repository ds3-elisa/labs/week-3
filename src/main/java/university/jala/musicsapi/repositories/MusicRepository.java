package university.jala.musicsapi.repositories;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;
import university.jala.musicsapi.models.MusicModel;
import org.springframework.stereotype.Repository;

import java.util.List;
import java.util.UUID;


@Repository
public interface MusicRepository extends JpaRepository<MusicModel, UUID> {

    @Query("SELECT DISTINCT m.genre FROM MusicModel m")
    List<String> findAllGenres();
    List<MusicModel> findByGenre(String genre);

    List<MusicModel> findByArtist(String artist);

    List<MusicModel> findAllByOrderByTitle();

}
