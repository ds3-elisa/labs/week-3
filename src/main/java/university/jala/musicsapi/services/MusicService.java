package university.jala.musicsapi.services;


import org.springframework.beans.BeanUtils;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.hateoas.Link;
import org.springframework.hateoas.server.mvc.WebMvcLinkBuilder;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.stereotype.Service;

import university.jala.musicsapi.controller.MusicController;
import university.jala.musicsapi.dtos.MusicRecordDto;
import university.jala.musicsapi.models.MusicModel;
import university.jala.musicsapi.repositories.MusicRepository;

import java.util.List;
import java.util.Optional;
import java.util.UUID;

@Service
public class MusicService {

    private final MusicRepository musicRepository;

    @Autowired
    public MusicService(MusicRepository musicRepository) {
        this.musicRepository = musicRepository;
    }

    public ResponseEntity<MusicModel> createMusic(MusicRecordDto musicRecordDto) {
        var musicModel = new MusicModel();
        BeanUtils.copyProperties(musicRecordDto, musicModel);
        MusicModel savedMusic = musicRepository.save(musicModel);
        return ResponseEntity.status(HttpStatus.CREATED).body(savedMusic);
    }

    public ResponseEntity<List<MusicModel>> searchAllMusics() {
        List<MusicModel> musicList = musicRepository.findAll();
        return ResponseEntity.status(HttpStatus.OK).body(musicList);
    }

    public List<String> searchAllGenres() {
        return musicRepository.findAllGenres();
    }

    public List<MusicModel> searchMusicByGenre(String genre) {
        return musicRepository.findByGenre(genre);
    }

    public List<MusicModel> searchMusicByArtist(String artist) {
        return musicRepository.findByArtist(artist);
    }

    public List<MusicModel> getAllMusicsSortedByTitle() {
        return musicRepository.findAllByOrderByTitle();
    }

    public ResponseEntity<Object> searchMusic(UUID id) {
        Optional<MusicModel> musicO = musicRepository.findById(id);

        if (musicO.isEmpty()) {
            return ResponseEntity.status(HttpStatus.NOT_FOUND).body("Music Not Found");
        }

        MusicModel music = musicO.get();
        Link link = WebMvcLinkBuilder.linkTo(WebMvcLinkBuilder.methodOn(MusicController.class).getAllMusics()).withRel("allMusics");
        music.add(link);

        return ResponseEntity.status(HttpStatus.OK).body(music);
    }

    public MusicModel updateMusic(UUID id, MusicRecordDto musicRecordDto) {
        Optional<MusicModel> musicOptional = musicRepository.findById(id);

        if (musicOptional.isEmpty()) {
            return null;
        }

        MusicModel musicModel = musicOptional.get();
        BeanUtils.copyProperties(musicRecordDto, musicModel);
        return musicRepository.save(musicModel);
    }

    public boolean deletedMusic(UUID id) {
        Optional<MusicModel> musicOptional = musicRepository.findById(id);

        if (musicOptional.isEmpty()) {
            return false;
        }

        musicRepository.delete(musicOptional.get());
        return true;
    }
}
